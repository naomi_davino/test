<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert(
                        [
                            [
                               'name' => 'A',
                               'user_id'=> '1',
                                
                               'created_at'=> date('Y-m-d G:i:s'),
                            ],
                            [
                                'name' => 'B',
                                'user_id'=> '1', 
                            
                                'created_at'=> date('Y-m-d G:i:s'),
                             ],
                             [
                           'name' => 'C',
                                'user_id'=> '1',
                                 
                                'created_at'=> date('Y-m-d G:i:s'),
                             ],
                        ]
                    );
            
    }
}
