<?php

use App\Customer;
use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/read', function(){
    
        $customers = Customer::all(); 
        foreach($customers as $customer){
    
            echo $customer->name ."<br>";
            
        }
    
    
    });
    
    Route::get('/customers', function(){
     
        $user = User::find(1);
    
        foreach($user->customers as $customer){
          echo  $customer->name ."<br>";
        
        }
    });



Route::get('/customers', 'CustomerController@index');
Route::resource('customers','CustomerController')->middleware('auth');
Auth::routes();