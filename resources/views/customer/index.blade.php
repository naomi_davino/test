<!DOCTYPE html>
<html>
<h1> this is customer</h1>
    <head>
    </head>
    <body>
    <a href="{{route('customers.create')}}">Create a new customer</a>
    <table>
            <tr>
                <th>name</th>
                <th>email</th>
                <th>phone number</th>
           </tr>
          
          @foreach($customers as $customer)
            <tr>
                <td>{{$customer->name}}</td>
                <td>{{$customer->email}}</td>
                <td>{{$customer->phone}}</td>
                <td><a href="{{route('customers.edit',$customer->id)}}">edit</a> <td>
                <td> 
                @can('manager') <td>delete</td>@cannot('manager')
    <form method = 'post' action="{{action('CustomerController@destroy', $customer->id)}}">
       @csrf
       @method('DELETE')
       
            <div class = "form-group">
               <input type ="submit" class = "form-control" name="submit" value ="Delete customer">
            </div>
      </form></td>
      @endcan
     @endcannot
            </td>
            <td> 
            @foreach($users as $user)
            {{$user->name}}
            @endforeach</td>

             @endforeach
        </table>
        <script>
      $(document).ready(function(){
           $(":checkbox").click(function(event){
              console.log(event.target.id)
              $.ajax({
                   url: "{{url('customers')}}"+'/' + event.target.id,
                   dataType: 'json',
                   type: 'put' ,
                  contentType:'application/json',
                   data:JSON.stringify({'status':event.target.checked, _token:"{{csrf_token()}}"}),
                   processData:false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>
  
   </body>
</html>